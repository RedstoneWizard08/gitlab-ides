# frozen_string_literal: true

resources :trials, only: [:new, :create]
